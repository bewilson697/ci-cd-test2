terraform {
   backend "s3" {
    
    bucket = "ci-cd-test-03-01"
    key    = "default.tfstate"
    region = "us-east-1"
  }
} 
